package io.piveau.consus.udata;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.piveau.consus.response.UDataResponse;
import io.piveau.pipe.PipeContext;
import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.*;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.HttpRequest;
import io.vertx.ext.web.client.WebClient;
import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.config.builders.ExpiryPolicyBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.config.units.EntryUnit;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public class ImportingUDataVerticle extends AbstractVerticle {

    public static final String ADDRESS = "io.piveau.pipe.importing.udata.queue";

    private WebClient client;

    private Cache<String, JsonObject> cache;

    private int defaultDelay;

    @Override
    public void start(Promise<Void> startPromise) {
        vertx.eventBus().consumer(ADDRESS, this::handlePipe);
        client = WebClient.create(vertx);

        CacheManager cacheManager = CacheManagerBuilder.newCacheManagerBuilder()
                .withCache("udataZones", CacheConfigurationBuilder.newCacheConfigurationBuilder(String.class, JsonObject.class,
                        ResourcePoolsBuilder.newResourcePoolsBuilder().heap(2000, EntryUnit.ENTRIES))
                        .withExpiry(ExpiryPolicyBuilder.timeToIdleExpiration(Duration.ofHours(12))))
                .build(true);

        cache = cacheManager.getCache("udataZones", String.class, JsonObject.class);

        ConfigStoreOptions envStoreOptions = new ConfigStoreOptions()
                .setType("env")
                .setConfig(new JsonObject().put("keys", new JsonArray().add("PIVEAU_IMPORTING_SEND_LIST_DELAY")));
        ConfigRetriever retriever = ConfigRetriever.create(vertx, new ConfigRetrieverOptions().addStore(envStoreOptions));
        retriever.getConfig()
                .onSuccess(config -> {
                    defaultDelay = config.getInteger("PIVEAU_IMPORTING_SEND_LIST_DELAY", 8000);
                    startPromise.complete();
                })
                .onFailure(startPromise::fail);
    }

    private void handlePipe(Message<PipeContext> message) {
        PipeContext pipeContext = message.body();
        JsonObject config = pipeContext.getConfig();
        pipeContext.log().info("Import started.");

        String address = config.getString("address");
        fetchPage(address + "/api/1/datasets/?page_size=100&page=1", pipeContext, new ArrayList<>());
    }

    private void fetchPage(String address, PipeContext pipeContext, List<String> identifiers) {
        HttpRequest<Buffer> request = client.getAbs(address);
        request.send()
                .onSuccess(response -> {
                    UDataResponse uDataResponse = new UDataResponse(response.bodyAsJsonObject());
                    if (uDataResponse.isSuccess()) {
                        JsonObject content = uDataResponse.getResult().getContent();
                        JsonArray data = content.getJsonArray("data", new JsonArray());

                        long total = content.getLong("total");
                        data.forEach(object -> forwardDataset((JsonObject) object, pipeContext, total, identifiers));

                        String nextPage = content.getString("next_page");
                        if (nextPage != null && !nextPage.isEmpty()) {
                            fetchPage(nextPage, pipeContext, identifiers);
                        } else {
                            pipeContext.log().info("Import metadata finished");
                            int delay = pipeContext.getConfig().getInteger("sendListDelay", defaultDelay);
                            vertx.setTimer(delay, t -> {
                                ObjectNode info = new ObjectMapper().createObjectNode()
                                        .put("content", "identifierList")
                                        .put("catalogue", pipeContext.getConfig().getString("catalogue"));
                                pipeContext.setResult(new JsonArray(identifiers).encodePrettily(), "application/json", info).forward();
                            });
                        }
                    } else {
                        pipeContext.setFailure(uDataResponse.getError().getMessage());
                    }
                })
                .onFailure(pipeContext::setFailure);
    }

    private void forwardDataset(JsonObject dataset, PipeContext pipeContext, long total, List<String> identifiers) {
        String address = pipeContext.getConfig().getString("address");
        completeSpatialInfo(address, dataset)
                .onComplete(ar -> {
                    identifiers.add(dataset.getString("slug"));
                    ObjectNode dataInfo = new ObjectMapper().createObjectNode()
                            .put("total", total)
                            .put("counter", identifiers.size())
                            .put("identifier", dataset.getString("slug"))
                            .put("catalogue", pipeContext.getConfig().getString("catalogue"));
                    pipeContext.setResult(dataset.encodePrettily(), "application/json", dataInfo).forward();
                    if (ar.succeeded()) {
                        pipeContext.log().info("Data imported: {}", dataInfo);
                    } else {
                        pipeContext.log().warn("Data may imported incomplete: {}", dataInfo);
                    }
                });
    }

    private Future<Void> completeSpatialInfo(String address, JsonObject dataset) {
        return Future.future(promise -> {
            JsonObject spatial = dataset.getJsonObject("spatial");
            if (spatial != null) {
                JsonArray zones = spatial.getJsonArray("zones", new JsonArray());
                if (zones == null || zones.isEmpty()) {
                    promise.complete();
                } else {
                    List<Future<JsonObject>> futures = new ArrayList<>();
                    zones.forEach(obj -> {
                        String zone = obj.toString();
                        if (!zone.startsWith("country")) {
                            Future<JsonObject> future = fetchZone(address, zone);
                            futures.add(future);
                        }
                    });
                    if (!futures.isEmpty()) {
                        CompositeFuture.all(new ArrayList<>(futures)).onComplete(fut -> {
                            JsonArray geometries = new JsonArray();
                            futures.stream().filter(Future::succeeded).forEach(f -> {
                                geometries.add(f.result());
                            });
                            spatial.put("geom", geometries);
                            promise.complete();
                        });
                    } else {
                        promise.complete();
                    }
                }
            } else {
                promise.fail("Missing spatial object");
            }
        });
    }

    private Future<JsonObject> fetchZone(String address, String zone) {
        return Future.future(promise -> {
            if (cache.containsKey(zone)) {
                promise.complete(cache.get(zone));
            } else {
                client.getAbs(address + "/api/1/spatial/zones/" + URLEncoder.encode(zone, StandardCharsets.UTF_8))
                        .send()
                        .onSuccess(response -> {
                            JsonObject result = response.bodyAsJsonObject();
                            JsonArray features = result.getJsonArray("features", new JsonArray());
                            if (!features.isEmpty()) {
                                JsonObject geometry = features.getJsonObject(0).getJsonObject("geometry");
                                cache.put(zone, geometry);
                                promise.complete(geometry);
                            } else {
                                promise.fail("no geometry found");
                            }

                        })
                        .onFailure(promise::fail);
            }
        });
    }

}