# ChangeLog

## Unreleased

## 1.1.2 (2022-12-17)

**Changed:**
* Complete implementation switched to Futures 

## 1.1.1 (2021-06-05)

**Changed:**
* Lib dependencies

## 1.1.0 (2021-01-31)

**Changed:**
* Switched to Vert.x 4.0.0

## 1.0.3 (2020-09-03)

**Fixed:**
* Kotlin update issue

## 1.0.2 (2020-06-18)

**Changed:**
* Serialize pipe `startTime` as ISO standard string

## 1.0.1 (2020-02-28)

**Changed:**
* Lib update

## 1.0.0 (2019-11-08)

**Added:**
* buildInfo.json for build info via `/health` path
* config.schema.json
* `PIVEAU_LOG_LEVEL` for configuration of the general log level of the `io.piveau` package
* Configuration change listener

**Changed:**
* `PIVEAU_` prefix to logstash configuration environment variables
* Requires now latest LTS Java 11
* Docker base image to openjdk:11-jre

**Fixed:**
* Update all dependencies

## 0.1.0 (2019-05-17)

**Added:**
* `catalogue` read from configuration and pass it to the info object
* Environment `PIVEAU_IMPORTING_SEND_LIST_DELAY` for a configurable delay
* `sendListDelay` pipe configuration option

**Changed:**
* Readme

**Removed:**
* `mode` configuration and fetchIdentifier

**Fixed:**
* Finally, honor `outputFormat`

## 0.0.1 (2019-05-03)

Initial release
